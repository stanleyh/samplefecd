var app = angular.module('myApp', ['angular-carousel']);
//var events;
// var jsonFlickrApi = function(){};
var fycdController = app.controller('myCtrl', ['$scope', '$http', function($scope, $http) {

  var blogApiUrl = "http://blog-fecd.rhcloud.com/?json=get_recent_posts&count=8&callback=JSON_CALLBACK";
  $http.jsonp(blogApiUrl).
    success(function(blogData) {
      $scope.events = blogData.posts;
      $scope.imgs = [];

      angular.forEach($scope.events, function(event) {
        var theData = event.content;
        var v = theData.indexOf("photosetId:");

        if (v < 0) {

        } else {
          var sub = theData.substring(v+13);
          var i = sub.indexOf("'");
          var thePhotosetId = sub.substring(0,i);

          var flickrApiUrl = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&photoset_id="+thePhotosetId+"&api_key=e54499be5aedef32dccbf89df9eaf921&format=json&jsoncallback=JSON_CALLBACK";

          $http.jsonp(flickrApiUrl).
            success(function(data) {
              var photos = data.photoset.photo;

              if (photos.length > 0) {

                var rnd = Math.floor((Math.random() * photos.length) + 1);

                $("#img"+event.id).attr("src","http://farm"+photos[rnd].farm+".static.flickr.com/"+photos[rnd].server+"/"+photos[rnd].id+"_"+photos[rnd].secret+"_q.jpg");

                var imgs = {};
                rnd = Math.floor(Math.random() * photos.length);
                imgs.src = "http://farm"+photos[rnd].farm+".static.flickr.com/"+photos[rnd].server+"/"+photos[rnd].id+"_"+photos[rnd].secret+"_q.jpg"
                imgs.id = event.id;
                imgs.title = event.title;
                $scope.imgs.push(imgs);
              }
            });
        }
      });

      setTimeout(function() {
        $(".carousel").carousel();
      }, 5000);
    });

}]);
fycdController.directive('fycdheader', function() {
  return {
    restrict: 'EA',
    templateUrl: 'header.html'
  };
});
fycdController.directive('fycdmain', function() {
  return {
    restrict: 'EA',
    templateUrl: 'main.html'
  };
});
fycdController.directive('fycdlatestnews', function() {
  return {
    restrict: 'E',
    templateUrl: 'latestNews.html'
  };
});
fycdController.directive('fycdcarousel', function() {
  return {
    restrict: 'E',
    templateUrl: 'carousel.html'
  };
});
fycdController.directive('fycdevents', function() {
  return {
    restrict: 'E',
    templateUrl: 'events.html'
  };
});
fycdController.directive('fycdfooter', function() {
  return {
    restrict: 'E',
    templateUrl: 'footer.html'
  };
});



$(document).ready(function() {
  var $nav = $("div[role='navigation']");

});
