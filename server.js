#!/bin/env node
var http = require('http'),
  express = require('express'),
  logger = require('morgan'),
  app = express(),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser');

//Get the environment variables we need.
var ipaddr = process.env.OPENSHIFT_NODEJS_IP || "192.168.42.88";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
//app.use(express.session({secret: 'secretKey', key: 'express.sid', myid: 'cool'}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// no stack traces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500)
       .send({ message: err.message });
});

// special routing
//app.get('/sendemail', sendemail.send);


// start http server
app.listen(port);
console.log("Server running at http://" + ipaddr + ":" + port + "/");
