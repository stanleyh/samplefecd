## Prerequisites

You must have [**node**](http://nodejs.org/) installed.

# Run it

* open a terminal or command window
* go to the main directory
* run `npm install` - to install required node-modules
* run `node server` - to start node
* go to http://127.0.0.1:8080/